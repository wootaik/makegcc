/***************************************************************************//*!
*
* @file     MathTestRunner.c
*
* @author
*
* @version
*
* @date
*
* @brief
*
******************************************************************************/

#include "unity_fixture.h"

TEST_GROUP_RUNNER(Math)
{
    RUN_TEST_CASE(Math, I16Abs_Pos);
    RUN_TEST_CASE(Math, I16Abs_Neg);
    RUN_TEST_CASE(Math, I16Abs_NegOutOfRange);

    RUN_TEST_CASE(Math, I16AbsSat_Pos);
    RUN_TEST_CASE(Math, I16AbsSat_Neg);
    RUN_TEST_CASE(Math, I16AbsSat_NegOutOfRange);

    RUN_TEST_CASE(Math, I16Add_PosPos);
    RUN_TEST_CASE(Math, I16Add_NegNeg);
    RUN_TEST_CASE(Math, I16Add_PosPosOutOfRange);
    RUN_TEST_CASE(Math, I16Add_NegNegOutOfRange);

    RUN_TEST_CASE(Math, I16AddSat_PosPos);
    RUN_TEST_CASE(Math, I16AddSat_NegNeg);
    RUN_TEST_CASE(Math, I16AddSat_PosPosOutOfRange);
    RUN_TEST_CASE(Math, I16AddSat_NegNegOutOfRange);

}
