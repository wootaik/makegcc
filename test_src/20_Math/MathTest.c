/***************************************************************************//*!
*
* @file     MathTest.c
*
* @author
*
* @version
*
* @date
*
* @brief
*
******************************************************************************/

#include "Math.h"

#include "unity_fixture.h"

TEST_GROUP(Math);

TEST_SETUP(Math)
{
	;
}

TEST_TEAR_DOWN(Math)
{
    ;
}
/* I16Abs */
TEST(Math, I16Abs_Pos)
{
    TEST_ASSERT_EQUAL_INT16( 100, I16Abs(100));
}

TEST(Math, I16Abs_Neg)
{
    TEST_ASSERT_EQUAL_INT16( 100, I16Abs(-100));
}

TEST(Math, I16Abs_NegOutOfRange)
{
    TEST_ASSERT_EQUAL_INT16( -32768, I16Abs(-32768));
}

/* I16AbsSat */
TEST(Math, I16AbsSat_Pos)
{
    TEST_ASSERT_EQUAL_INT16( 100, I16AbsSat(100));
}

TEST(Math, I16AbsSat_Neg)
{
    TEST_ASSERT_EQUAL_INT16( 100, I16AbsSat(-100));
}

TEST(Math, I16AbsSat_NegOutOfRange)
{
    TEST_ASSERT_EQUAL_INT16( 32767, I16AbsSat(-32768));
}

/* I16Add */
TEST(Math, I16Add_PosPos)
{
    TEST_ASSERT_EQUAL_INT16( 200, I16Add(100,100));
}

TEST(Math, I16Add_NegNeg)
{
    TEST_ASSERT_EQUAL_INT16( -200, I16Add(-100,-100));
}

TEST(Math, I16Add_PosPosOutOfRange)
{
    TEST_ASSERT_EQUAL_INT16( -32768, I16Add(32767,1));
}

TEST(Math, I16Add_NegNegOutOfRange)
{
    TEST_ASSERT_EQUAL_INT16( 32767, I16Add(-32768,-1));
}

/* I16AddSat */
TEST(Math, I16AddSat_PosPos)
{
    TEST_ASSERT_EQUAL_INT16( 200, I16AddSat(100,100));
}

TEST(Math, I16AddSat_NegNeg)
{
    TEST_ASSERT_EQUAL_INT16( -200, I16AddSat(-100,-100));
}

TEST(Math, I16AddSat_PosPosOutOfRange)
{
    TEST_ASSERT_EQUAL_INT16( 32767, I16AddSat(32767,1));
}

TEST(Math, I16AddSat_NegNegOutOfRange)
{
    TEST_ASSERT_EQUAL_INT16( -32768, I16AddSat(-32768,-1));
}

/* End of file */

