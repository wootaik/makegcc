/***************************************************************************//*!
*
* @file     UserLibTestRunner.c
*
* @author
*
* @version
*
* @date
*
* @brief
*
******************************************************************************/

#include "unity_fixture.h"

TEST_GROUP_RUNNER(UserLib)
{
    RUN_TEST_CASE(UserLib, HysteresisInitial2High);
    RUN_TEST_CASE(UserLib, HysteresisHigh2Low);
    RUN_TEST_CASE(UserLib, HysteresisLow2High);
    RUN_TEST_CASE(UserLib, RampIncrement);
    RUN_TEST_CASE(UserLib, RampDecrement);
    RUN_TEST_CASE(UserLib, RampSwing);

}
