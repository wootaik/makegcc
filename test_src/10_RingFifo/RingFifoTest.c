/***************************************************************************//*!
*
* @file     RingFifoTest.c
*
* @author
*
* @version
*
* @date
*
* @brief    Source file containing routines ....
*
******************************************************************************/

/******************************************************************************
| Includes
-----------------------------------------------------------------------------*/

#include "MyModule.h"

int main(void)
{
	int16_t data;

	InitFifo(MyFifo, &MyIdx, MY_FIFO_SIZE);
	PrintFifo(MyFifo, &MyIdx);

	PutFifo(MyFifo, &MyIdx, 1);
	PutFifo(MyFifo, &MyIdx, 2);
	PutFifo(MyFifo, &MyIdx, 3);
	PutFifo(MyFifo, &MyIdx, 4);
	PutFifo(MyFifo, &MyIdx, 5);
	PrintFifo(MyFifo, &MyIdx);

	GetFifo(MyFifo, &MyIdx, &data);
	printf("data = %4d\n", data);
	GetFifo(MyFifo, &MyIdx, &data);
	printf("data = %4d\n", data);
	GetFifo(MyFifo, &MyIdx, &data);
	printf("data = %4d\n", data);

	PutFifo(MyFifo, &MyIdx, 6);
	PutFifo(MyFifo, &MyIdx, 7);
	PutFifo(MyFifo, &MyIdx, 8);
	PutFifo(MyFifo, &MyIdx, 9);
	PutFifo(MyFifo, &MyIdx, 10);
	PutFifo(MyFifo, &MyIdx, 11);
	PrintFifo(MyFifo, &MyIdx);

	return(0);
}
/* End of file */

