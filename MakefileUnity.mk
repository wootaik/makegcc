#This makefile makes the examples from the first few chapters with Unity test harness

#Set this to @ to keep the makefile quiet
SILENCE = @

#---- Outputs ----#
COMPONENT_NAME = MyUnity

#--- Inputs ----#
PROJECT_HOME_DIR = .
PROJECT_SRC_DIR = $(PROJECT_HOME_DIR)/src
PROJECT_TEST_DIR = $(PROJECT_HOME_DIR)/test_src
UNITY_HOME = lib/unity

UNITY_CFLAGS += -DUNITY_OUTPUT_CHAR=UnityOutputCharSpy_OutputChar
UNITY_WARNINGFLAGS = -Wall -Werror -Wswitch-default
#UNITY_WARNINGFLAGS += -Wshadow 

SRC_DIRS = \
	$(PROJECT_SRC_DIR)/20_Math \
	$(PROJECT_SRC_DIR)/30_UserLib \

TEST_SRC_DIRS = \
	$(UNITY_HOME)\
	$(UNITY_HOME)/src\
    $(UNITY_HOME)/extras/fixture/src\
    $(UNITY_HOME)/extras/fixture/test\
	$(PROJECT_TEST_DIR)\
	$(PROJECT_TEST_DIR)/20_Math\
	$(PROJECT_TEST_DIR)/30_UserLib\

MOCKS_SRC_DIRS = \
#	$(PROJECT_TEST_DIR)/mocks\

INCLUDE_DIRS =\
	.\
	$(UNITY_HOME)/src\
	$(UNITY_HOME)/extras/fixture/src\
	$(UNITY_HOME)/extras/fixture/test\
	$(PROJECT_HOME_DIR)/include\
	$(PROJECT_HOME_DIR)/src/20_Math\
	$(PROJECT_HOME_DIR)/src/30_UserLib\
#	$(PROJECT_HOME_DIR)/src/util\
#	$(PROJECT_TEST_DIR)/mocks\
   
include build/MakefileWorker.mk

