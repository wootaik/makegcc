################################################################################
#
#  file         BuildConfig.mk
#
#  brief        Configuration definitions for compiler and target board  
#                                                                              
#  version      1.00
#  date         2015/07/01
#  author       Wootaik Lee <wootaik@gmail.com>
#  warning
#  bug
#  note 
# 
# ---------------------------------------------------------------------------
#               R E V I S I O N   H I S T O R Y                              
# ---------------------------------------------------------------------------
#   Date        Version  Author   Description	                              
#  ----------   -------  -------  -------------------------------------------
#  2015-07-01   1.00     W. Lee   - Creation for Gcc, Unity, gcov, lcov
#
################################################################################

# Target Name ##################################################################
TRGT = MyTarget

# Project PATH #################################################################
#PROJPATH = "D:/workspace/TestEmbeddedC/MakeGcc"

# Compiler #####################################################################
# Define the compiler.
# 	COMPILER_GCC : for PC
COMPILER=COMPILER_GCC

include $(BASEDIR)/build/compiler/$(COMPILER).mk
