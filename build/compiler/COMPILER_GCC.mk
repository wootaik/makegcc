################################################################################
#
#  file         COMPILER_GCC.defs
#
#  brief        Configuration definitions for gcc compiler  
#                                                                              
#  version      1.00
#  date         2015/07/01
#  author       Wootaik Lee <wootaik@changwon.ac.kr>
#  warning
#  bug
#  note 
# 
# ---------------------------------------------------------------------------
#               R E V I S I O N   H I S T O R Y                              
# ---------------------------------------------------------------------------
#   Date        Version  Author   Description	                              
#  ----------   -------  -------  -------------------------------------------
#  2015-07-01   1.00     W. Lee   - Creation Prerelease                      
#
################################################################################

# File location ################################################################
# Place to put object and binary codes
OBJDIR=$(SRCDIR)/obj

# GCC Path #####################################################################
# GCC Tool 
CC=gcc.exe
AS=gcc.exe
LD=gcc.exe

RM=rm -f

# Preprocess ###################################################################
# file includes 
# INCDIR : project include directory
# SYSINCS : compiler lib include directory
# INCLUDES : option for include and include 'INCDIR' and 'SYSINCS'
#      -I- : change target for '-I' access paths to the system list
#            only system path are searched with '#include <...>' while compiling
#      -I path : append access path to current #include list
INCDIR=-I$(BASEDIR)/include/ 
SYSINCS = 

INCLUDES+= -I. $(INCDIR) $(SYSINCS)

# CPP flags passed during a compilation (include paths)
# COMPILER : defined in config/make.defs
#     -D : define symbol
CPPFLAGS=$(INCLUDES) -D$(COMPILER)

# Compile ######################################################################
# Basic Compiler option
# -fmessage-length=n : Try to format error messages so that they fit on lines
#                      of about n characters.
# -MMD : Generate a dependency output file, and applies a '.d' suffix.
#        (not include system head files informations)
# -MP : This option instrcts CPP to add a phony target for each dependency
#       other than the main file, causing each to depend on nothing.
COMPILE=  -c -fmessage-length=0 -MMD -MP

# Debug option
# -g[level] : Request debugging information as level
#    -g0 : Produces no debug information al all.
#    -g1 : Produces minimal information.
#          Includes descriptions of functions and external variables, 
#          but no information about local variables and no line numbers
#    -g2 : default debugging information level
#    -g3 : includes extra information,
#          such as macro definitions present in the program. 
DEBUG=-g3

# Define the optimization you want:
# -O[level] : Optimize the code as level
#    -O0 : Reduce compilation time
#          and make debugging produce the expected results.
#    -O1 : Optimizing compilation takes somewhat more time,
#          and a lot more memory for a large function.
#    -O2 : Optimize even more.
#          GCC performs nearly all supported optimizations that
#          do not involve a space-speed tradeoff.
#    -O3 : Optimize yet more.
#    -Os : Optimize for size
OPT=-O0

# Generate full warnings
# -Wall : Turns on all optional warnings which are desirable for normal code
# -Wcomments : Warn whenever a comment-start sequence such as '/*' or '//'
# -Werror : Make all warnings into hard errors
WARN=-Wall

# C flags used by default to compile a program
CFLAGS=$(WARN) $(DEBUG) $(OPT) $(COMPILE)

# AS flags used by default to assembler a program
ASFLAGS=$(WARN) $(DEBUG) $(OPT) $(COMPILE)

# Link #########################################################################
# Basic Linking option

LINK= 

# Compiler Library 
LIBS+=

              
LDFLAGS = $(LINK) $(LIBS) 

# Relation #####################################################################
.c.o:
	$(CC) -c $(CFLAGS) $(CPPFLAGS) -o $(OBJDIR)/$@ $<

.asm.o:
	$(AS) -c $(ASFLAGS) $(CPPFLAGS) -o $(OBJDIR)/$@ $<

all::


clean::
	$(RM) $(OBJDIR)/*.o
	$(RM) $(OBJDIR)/*.d	
	$(RM) $(OBJDIR)/*.exe
#	$(RM) $(OBJDIR)/*.elf
#	$(RM) $(OBJDIR)/*.map
	