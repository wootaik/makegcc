
#include "typedefs.h"

#define MY_FIFO_SIZE	10

typedef struct
{
  uint16_t PutIdx;
  uint16_t GetIdx;
  uint16_t UsedSize;
  uint16_t FifoSize;
}
RingFifoIdx;

int16_t MyFifo[MY_FIFO_SIZE];
RingFifoIdx MyIdx;

void InitFifo(int16_t fifo[], RingFifoIdx * idx, uint16_t size) ;

int16_t PutFifo (int16_t fifo[], RingFifoIdx * idx, int16_t data);

int16_t GetFifo (int16_t fifo[], RingFifoIdx * idx, int16_t * data);

void InitFifo(int16_t fifo[], RingFifoIdx * idx, uint16_t size)
{
	int16_t i;
	for(i=0 ; i<size ; i++)
	{
		fifo[i]=0;
	}
	idx->PutIdx = 0;
	idx->GetIdx = 0;
	idx->UsedSize = 0;
	idx->FifoSize = size;
}

int16_t PutFifo (int16_t fifo[], RingFifoIdx * idx, int16_t data)
{
	fifo[idx->PutIdx] = data;
	idx->PutIdx++;
	if (idx->PutIdx == idx->FifoSize)
	{
		idx->PutIdx = 0; /* Wrap */
	}
	idx->UsedSize ++;

	return(0);

}

int16_t GetFifo (int16_t fifo[], RingFifoIdx * idx, int16_t * data)
{
	*data = fifo[idx->GetIdx];
	idx->GetIdx++;
	if (idx->GetIdx == idx->FifoSize)
	{
		idx->GetIdx = 0; /* Wrap */
	}
	idx->UsedSize --;

	return(0);

}

void PrintFifo(int16_t fifo[], RingFifoIdx * idx)
{
	int16_t i;
	printf("PutIdx:%4d, GetIdx:%4d, UsedSize: %4d\n", idx->PutIdx, idx->GetIdx, idx->UsedSize);
	for(i = 0; i< idx->FifoSize; i++)
	{
		printf("%4d",fifo[i]);
	}
	printf("\n");
}


