/***************************************************************************//*!
*
* @file     CheckCompiler.c
*
* @author
*
* @version
*
* @date
*
* @brief    Source file containing routines ....
*
******************************************************************************/

/******************************************************************************
| Includes
-----------------------------------------------------------------------------*/
#include "CheckCompiler.h"

#include <stdio.h>

/******************************************************************************
| External declarations
-----------------------------------------------------------------------------*/

/******************************************************************************
| Defines and macros            (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Typedefs and structures       (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Global variable definitions   (scope: module-exported)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Global variable definitions   (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Function prototypes           (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Function implementations      (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Function implementations      (scope: module-exported)
-----------------------------------------------------------------------------*/

/**************************************************************************//*!
\nosubgrouping
@brief      The function check size of basic type

@details

@note       All parameters and states, used by the function, can be reset...
******************************************************************************/

void CheckSize(void)
{
  printf("Size of char      :%3d\n", sizeof(char));
  printf("Size of short     :%3d\n", sizeof(short));
  printf("Size of int       :%3d\n", sizeof(int));
  printf("Size of long      :%3d\n", sizeof(long));
  printf("Size of long long :%3d\n", sizeof(long long));
  printf("Size of float     :%3d\n", sizeof(float));
  printf("Size of double    :%3d\n", sizeof(double));
  printf("Size of long double    :%3d\n", sizeof(long double));
}

/**************************************************************************//*!
\nosubgrouping
@brief      The function check integer division

@details

@note       All parameters and states, used by the function, can be reset...
******************************************************************************/
void CheckDivision(void)
{
	int q = 0;
	int r = 0;

	q = 5 / 3;
	r = 5 % 3;

	q = -5 / 3;
	r = -5 % 3;

	q = 5 / -3;
	r = 5 % -3;

	q = -5 / -3;
	r = -5 % -3;
}

/**************************************************************************//*!
\nosubgrouping
@brief      The function check bit field

@details

@note       All parameters and states, used by the function, can be reset...
******************************************************************************/
void CheckBitField(void)
{
	struct bit_field
	{
		int16_t	  First4: 	4;
		int16_t	  Second2: 	2;
		uint16_t  Third1:	1;
		uint16_t  Fourth1:	1;
	} my_bit_field;

	my_bit_field.First4 = 15;
	my_bit_field.Second2 = 2;
	my_bit_field.Third1 = 1;
	my_bit_field.Fourth1 = 1;

	my_bit_field.First4 = 0;
	my_bit_field.Second2 = 0;
	my_bit_field.Third1 = 0;
	my_bit_field.Fourth1 = 0;

	my_bit_field.First4 = my_bit_field.Second2
			+ my_bit_field.Third1 + my_bit_field.Fourth1;
}

/**************************************************************************//*!
\nosubgrouping
@brief      The function check volatile

@details

@note       All parameters and states, used by the function, can be reset...
******************************************************************************/
void CheckVolatile(void)
{
	int i;
	volatile int vi;

	i=0;
	i=1;
	i=2;
	i=3;

	vi=0;
	vi=1;
	vi=2;
	vi=3;

}
/* End of file */

