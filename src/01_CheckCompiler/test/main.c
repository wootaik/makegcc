/*
 * main.c
 *
 *  Created on: 2015. 1. 15.
 *      Author: Wootaik
 */

#include "CheckCompiler.h"

int main(void){

	CheckSize();
	CheckDivision();
	CheckBitField();
	CheckVolatile();

	return (0);
}
