/***************************************************************************//*!
*
* @file     MyModule.h
*
* @author
*
* @version
*
* @date
*
* @brief    Header file for MyModule.
*
******************************************************************************/
#ifndef MYMODULE_H_
#define MYMODULE_H_

#include "typedefs.h"
#include "defines.h"

/******************************************************************************
| Defines and macros            (scope: module-exported)
-----------------------------------------------------------------------------*/


/******************************************************************************
| Typedefs and structures       (scope: module-exported)
-----------------------------------------------------------------------------*/


/******************************************************************************
| Exported Variables
-----------------------------------------------------------------------------*/


/******************************************************************************
| Exported function prototypes
-----------------------------------------------------------------------------*/
int32_t MyFunction(int32_t s32In, int32_t *pParam);

#endif /* MYMODULE_H_ */
