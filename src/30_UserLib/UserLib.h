/***************************************************************************//*!
*
* @file     UserLib.h
*
* @author
*
* @version
*
* @date
*
* @brief    Header file for UserLib.
*
******************************************************************************/
#ifndef USERLIB_H_
#define USERLIB_H_

#include "typedefs.h"
#include "defines.h"

/******************************************************************************
| Defines and macros            (scope: module-local)
-----------------------------------------------------------------------------*/
#define USERLIB_RAMP_DEFAULT	{0.0, 0.0, 0.0}

#define USERLIB_HYST_DEFAULT	{0.0, 0.0, 0.0, 0.0, 0.0}

/******************************************************************************
| Typedefs and structures       (scope: module-local)
-----------------------------------------------------------------------------*/
typedef struct
{
	float_t	fltState;
	float_t	fltIncrement;
	float_t	fltDecrement;
} UserLib_Ramp_t;

typedef struct
{
	float_t	fltHystOn;
	float_t	fltHystOff;
	float_t	fltOutOn;
	float_t	fltOutOff;
	float_t	fltOutState;
} UserLib_Hyst_t;

/******************************************************************************
| Exported Variables
-----------------------------------------------------------------------------*/


/******************************************************************************
| Exported function prototypes
-----------------------------------------------------------------------------*/
float_t UserLib_Ramp(float_t fltIn, UserLib_Ramp_t *pParam);
float_t UserLib_Hyst(float_t fltIn, UserLib_Hyst_t *pParam);

#endif /* USERLIB_H_ */
