/***************************************************************************//*!
*
* @file     UserLib.c
*
* @author
*
* @version
*
* @date
*
* @brief    Source file containing routines ....
*
******************************************************************************/

/******************************************************************************
| Includes
-----------------------------------------------------------------------------*/
#include "typedefs.h"
#include "defines.h"
#include "UserLib.h"



/******************************************************************************
| External declarations
-----------------------------------------------------------------------------*/

/******************************************************************************
| Defines and macros            (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Typedefs and structures       (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Global variable definitions   (scope: module-exported)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Global variable definitions   (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Function prototypes           (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Function implementations      (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Function implementations      (scope: module-exported)
-----------------------------------------------------------------------------*/

/**************************************************************************//*!

@brief      The function calculates the Increment/decrement ramp

@param[in]    fltIn      Input argument representing the desired output value.
@param[out]  *pParam     Pointer to parameters

@return     The function returns flt value, which represents
            the actual ramp output value

@details

@note       All parameters and states, used by the function, can be reset...
******************************************************************************/

float_t UserLib_Ramp(float_t fltIn, UserLib_Ramp_t *pParam)
{
	register float_t	flt_inc;
	register float_t 	flt_dec;
	register float_t	flt_state;

	flt_inc = pParam->fltState + pParam->fltIncrement;
	flt_dec = pParam->fltState + pParam->fltDecrement;

	flt_state = (pParam->fltState <= fltIn) ? flt_inc : flt_dec;

	pParam->fltState = (((flt_state >= fltIn) && (pParam->fltState <= fltIn)) ||
			 	 	   ((flt_state <= fltIn) && (pParam->fltState > fltIn))) ?
			 	 		fltIn : flt_state;
	return(pParam->fltState);

}

/**************************************************************************//*!

@brief      Hysteresis function

@param[in]   fltIn      Input argument
@param[out]  *pParam    Pointer to parameters

@return     The function returns flt value, which represents
            the actual output value

@details

@note       All parameters and states, used by the function, can be reset...
******************************************************************************/

float_t UserLib_Hyst(float_t fltIn, UserLib_Hyst_t *pParam)
{
	register float_t	flttemp;

	flttemp = (fltIn < pParam->fltHystOn) ? pParam->fltOutState : pParam->fltOutOn ;

	pParam->fltOutState = (fltIn <= pParam->fltHystOff) ? pParam->fltOutOff : flttemp ;

	return(pParam->fltOutState);

}
/* End of file */

