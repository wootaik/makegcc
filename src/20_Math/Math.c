/***************************************************************************//*!
*
* @file     MATH.c
*
* @author
*
* @version
*
* @date
*
* @brief    Source file containing routines ....
*
******************************************************************************/

/******************************************************************************
| Includes
-----------------------------------------------------------------------------*/
#include "Math.h"

#include "typedefs.h"
#include "defines.h"


/******************************************************************************
| External declarations
-----------------------------------------------------------------------------*/

/******************************************************************************
| Defines and macros            (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Typedefs and structures       (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Global variable definitions   (scope: module-exported)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Global variable definitions   (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Function prototypes           (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Function implementations      (scope: module-local)
-----------------------------------------------------------------------------*/

/******************************************************************************
| Function implementations      (scope: module-exported)
-----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*//*!

@brief      Absolute value of 16-bit integer type.

@return     Absolute value of the input argument

@note       Overflow is NOT detected
-----------------------------------------------------------------------------*/
int16_t I16Abs(register int16_t x)
{
	return (int16_t) ((x<0) ? -(x) : (x) );
}

/*------------------------------------------------------------------------*//*!

@brief      Absolute value of 16-bit integer type.

@return     Absolute value of the input argument

@note       Overflow is detected and saturates it
-----------------------------------------------------------------------------*/
int16_t I16AbsSat(register int16_t x)
{
	register int32_t y;
	y = (int32_t) x < 0 ? -(int32_t) x : (int32_t) x;
	y = (y == -((int32_t) INT16_MIN)) ? (int32_t) INT16_MAX : y;
	return (int16_t) y;
}

/*------------------------------------------------------------------------*//*!

@brief      Add two i16

@return     Sum of the input arguments

@note       Overflow is NOT detected
-----------------------------------------------------------------------------*/
int16_t I16Add(register int16_t x, register int16_t y)
{
	register int32_t z;
	z = x+y;
	return (int16_t) z;
}

/*------------------------------------------------------------------------*//*!

@brief      Add with overflow control two i16

@return     Sum of the input arguments

@note       Overflow is detected and saturates it
-----------------------------------------------------------------------------*/
int16_t I16AddSat(register int16_t x, register int16_t y)
{
	register int32_t z;
	z = (int32_t) x + (int32_t) y;

	z = (z> (int32_t)INT16_MAX) ? (int32_t) INT16_MAX : z;
	z = (z< (int32_t)INT16_MIN) ? (int32_t) INT16_MIN : z;

	return (int16_t) z;
}

/*------------------------------------------------------------------------*//*!

@brief      Absolute value of 32-bit integer type.

@return     Absolute value of the input argument

@note       Overflow is NOT detected
-----------------------------------------------------------------------------*/
int32_t I32Abs(register int32_t x)
{
	return (x<0) ? -x : x ;
}

/*------------------------------------------------------------------------*//*!

@brief      Absolute value of 32-bit integer type.

@return     Absolute value of the input argument

@note       Overflow is detected and saturates it
-----------------------------------------------------------------------------*/
int32_t I32AbsSat(register int32_t x)
{
	register int32_t y;

	y = (x < 0) ? -x : x;
	y = (y == INT32_MIN) ? INT32_MAX : y;

	return y;
}
/*------------------------------------------------------------------------*//*!

@brief     Add two value of 32-bit integer type.

@return    Sum of the input arguments

@note      Overflow is detected and saturates it
-----------------------------------------------------------------------------*/
int32_t I32AddSat(register int32_t x, register int32_t y)
{
	register int32_t z;
	register int32_t sat_max, sat_min;

	z = x + y;

	sat_max = ~(x|y) & z;
	sat_min = x & y & ~z;

	z = (sat_min < 0) ? INT32_MIN : z;
	z = (sat_max < 0) ? INT32_MAX : z;

	return z;
}

/* End of file */

