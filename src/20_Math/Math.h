/***************************************************************************//*!
*
* @file     Math.h
*
* @author
*
* @version
*
* @date
*
* @brief    Header file for Math.
*
******************************************************************************/
#ifndef MATH_H_
#define MATH_H_

#include "typedefs.h"
#include "defines.h"

/******************************************************************************
| Defines and macros            (scope: module-local)
-----------------------------------------------------------------------------*/
/* Type casting */

/******************************************************************************
| Typedefs and structures       (scope: module-local)
-----------------------------------------------------------------------------*/


/******************************************************************************
| Exported Variables
-----------------------------------------------------------------------------*/


/******************************************************************************
| Exported function prototypes
-----------------------------------------------------------------------------*/
int16_t I16Abs(register int16_t x);
int16_t I16AbsSat(register int16_t x);
int16_t I16Add(register int16_t x, register int16_t y);
int16_t I16AddSat(register int16_t x, register int16_t y);

int32_t I32Abs(register int32_t x);
int32_t I32AbsSat(register int32_t x);
int32_t I32Add(register int32_t x, register int32_t y);
int32_t I32AddSat(register int32_t x, register int32_t y);


#endif /* MATH_H_ */
