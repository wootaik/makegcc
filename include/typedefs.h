/***************************************************************************//*!
*
* @file     typedefs.h
*
* @author   Wootaik Lee (WT, wootaik@changwon.ac.kr)
*
* @version  1.0
*
* @date     2015.06.28
*
* @brief    Data Types for general purpose.
*
******************************************************************************/

#ifndef _TYPEDEFS_H_
#define _TYPEDEFS_H_


/******************************************************************************
| Typedefs and structures
-----------------------------------------------------------------------------*/
#ifdef __MWERKS__    //Metrowerk CodeWarrior
#include <stdint.h>

typedef unsigned char   bool_t;


#else                // GCC ISO C Standard

#include <stdint.h>

typedef unsigned char           bool_t; /* Used for Boolean by enforcement examples */


#endif

typedef volatile int8_t   vint8_t;
typedef volatile uint8_t  vuint8_t;

typedef volatile int16_t  vint16_t;
typedef volatile uint16_t vuint16_t;

typedef volatile int32_t  vint32_t;
typedef volatile uint32_t vuint32_t;

typedef int16_t             frac16_t;        /*!< 16-bit signed fractional Q1.15 type */
typedef int32_t             frac32_t;        /*!< 32-bit Q1.31 type */
typedef float               float_t;

/******************************************************************************
| Defines and macros
-----------------------------------------------------------------------------*/
#ifndef FALSE
#define FALSE ((bool_t)0)                    /*!< Boolean type FALSE constant */
#endif

#ifndef TRUE
#define TRUE ((bool_t)1)                     /*!< Boolean type TRUE constant */
#endif

#ifndef NULL
#define NULL 0                     /*!< NULL constant */
#endif

#endif /* _TYPEDEFS_H_ */
