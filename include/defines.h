/***************************************************************************//*!
*
* @file     defines.h
*
* @author   Wootaik Lee (WT, wootaik@changwon.ac.kr)
*
* @version  1.0
*
* @date     2015.06.28
*
* @brief    Definitions for general purpose.
*
******************************************************************************/

#ifndef _DEFINES_H_
#define _DEFINES_H_

#include "typedefs.h"

/******************************************************************************
| Defines and macros            (scope: module-local)
-----------------------------------------------------------------------------*/
/*! Constant representing the maximal positive value of a unsigned 16-bit fixed point integer
 *  number, equal to 2\f$^{15}\f$ = 0x8000. */
#ifndef UINT16_MAX
#define UINT16_MAX          ((uint16_t) 0x8000)
#endif

/*! Constant representing maximal positive value of signed fixed point integer
 * 16-bit number e.q. \f$ 2^{16-1}-1 = \f$ 0x7fff. */
#ifndef INT16_MAX
#define INT16_MAX           ((int16_t) 0x7fff)
#endif

/*! Constant representing maximal negative value of signed fixed point integer
 * 16-bit number e.q. \f$ -2^{16-1} = \f$ 0x8000. */
#ifndef INT16_MIN
#define INT16_MIN           ((int16_t) 0x8000)
#endif

/*! Constant representing the maximal positive value of a unsigned 32-bit fixed point integer
 *  number, equal to 2\f$^{31}\f$ = 0x80000000. */
#ifndef UINT32_MAX
#define UINT32_MAX          ((uint32_t) 0x80000000U)
#endif

/*! Constant representing maximal positive value of signed fixed point integer
 * 32-bit number e.q. \f$ 2^{32-1}-1 = \f$ 0x7fff ffff. */
#ifndef INT32_MAX
#define INT32_MAX           ((int32_t) 0x7fffffff)
#endif

/*! Constant representing maximal negative value of signed fixed point integer
 * 32-bit number e.q. \f$ -2^{32-1} = \f$ 0x8000 0000. */
#ifndef INT32_MIN
#define INT32_MIN           ((int32_t) 0x80000000)
#endif

/*! Constant representing the maximal negative value of the 32-bit float type. */
#ifndef FLOAT_MIN
#define FLOAT_MIN           ((float_t)(-3.4028234e+38F))
#endif

/*! Constant representing the maximal positive value of the 32-bit float type. */
#ifndef FLOAT_MAX
#define FLOAT_MAX           ((float_t)(3.4028234e+38F))
#endif


#endif /* _DEFINES_H_ */
