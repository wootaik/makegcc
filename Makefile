
all: CodeUnity
clean: cleanCodeUnity
gcov: gcovCodeUnity
test: testCodeUnity
vtest: vtestCodeUnity

CodeUnity:
	make -i -f MakefileUnity.mk

cleanCodeUnity:
	make -i -f MakefileUnity.mk clean
	
gcovCodeUnity:
	make -i -f MakefileUnity.mk gcov
	
testCodeUnity: 
	make -i -f MakefileUnity.mk test

vtestCodeUnity: 
	make -i -f MakefileUnity.mk vtest
	